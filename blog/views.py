from django.shortcuts import render
from django.views import generic
from .models import *
# Create your views here.


class HomeView(generic.ListView):
	model = PostList
	context_object_name = 'all_posts'
	template_name = "blog/home.html"

	def get_queryset(self):
		return PostList.objects.all().order_by('-pub_date')

class ContactView(generic.ListView):
	template_name = "blog/contact.html"

	def get_queryset(self):
		return None

class AboutUsView(generic.ListView):
	template_name = "blog/about.html"

	def get_queryset(self):
		return None

class PostDetailView(generic.ListView):
	model = PostList
	template_name = "blog/blog-detail.html"

	def get(self, request, pk):
		return render(request, self.template_name, 
			{'post_details': PostList.objects.get(pk=pk)})