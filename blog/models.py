from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.urls import reverse



class PostList(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	post_title = models.CharField(max_length=100)
	post_content = models.TextField()
	pub_date = models.DateField(auto_now_add=True)


	def get_absolute_url(self):
		return reverse('blog:post_detail', kwargs={'pk':self.pk})

	def __str__(self):
		return self.post_title






